module github.com/just-arun/aerospike-db-session

go 1.16

require (
	github.com/aerospike/aerospike-client-go v4.5.0+incompatible // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-chi/chi/v5 v5.0.2 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/rs/cors v1.7.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
)
