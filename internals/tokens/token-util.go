package tokens

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/just-arun/aerospike-db-session/internals/config"
	"github.com/just-arun/aerospike-db-session/internals/stringutil"
)

type TokenType int

const (
	AccessToken TokenType = iota
	RefreshToken
)

type TokenResponse struct {
	AccessToken  string `json:"access"`
	RefreshToken string `json:"refresh"`
}

func GenerateToken(userId string) (TokenResponse, error) {
	hash := stringutil.EncryptString(config.GetEnv().AppSecret, userId)
	access, err := CreateToken(hash, AccessToken)
	if err != nil {
		return TokenResponse{}, err
	}
	refresh, err := CreateToken(hash, RefreshToken)
	if err != nil {
		return TokenResponse{}, err
	}
	return TokenResponse{
		AccessToken:  access,
		RefreshToken: refresh,
	}, nil
}

func CreateToken(payload string, tokenType TokenType) (string, error) {
	var expTime int64
	if tokenType == AccessToken {
		expTime = config.GetEnv().AccessTokenExpTime
	} else {
		expTime = config.GetEnv().RefreshTokenExpTime
	}
	var err error
	//Creating Access Token
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["uid"] = payload
	atClaims["exp"] = expTime
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(config.GetEnv().AppSecret))
	if err != nil {
		return "", err
	}
	return token, nil
}
