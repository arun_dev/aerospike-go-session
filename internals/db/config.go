package db

import (
	"fmt"

	aero "github.com/aerospike/aerospike-client-go"
	"github.com/just-arun/aerospike-db-session/internals/config"
)

var (
	ASClient *aero.Client
)

func Init() {
	initAerospike()
}

func initAerospike() {
	fmt.Println(
		config.GetEnv().AeroSpikeDBHost,
		config.GetEnv().AeroSpikeDBPort)
	client, err := aero.NewClient(
		config.GetEnv().AeroSpikeDBHost,
		config.GetEnv().AeroSpikeDBPort,
	)
	if err != nil {
		panic(err)
	}
	ASClient = client
}
