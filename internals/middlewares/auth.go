package middlewares

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	aero "github.com/aerospike/aerospike-client-go"
	"github.com/dgrijalva/jwt-go"
	"github.com/just-arun/aerospike-db-session/internals/config"
	"github.com/just-arun/aerospike-db-session/internals/db"
	"github.com/just-arun/aerospike-db-session/internals/response"
	"github.com/just-arun/aerospike-db-session/internals/stringutil"
	"github.com/just-arun/aerospike-db-session/internals/tokens"
	"github.com/just-arun/aerospike-db-session/internals/users"
)

type RequestValueKeyType int

type UserSessionObject struct {
	Data *users.User
}

const (
	RequestValueTypeUser RequestValueKeyType = iota
)

func getTokensFromRequest(r *http.Request) (accessToken, refreshToken string) {
	access := ""
	refresh := ""
	for _, cookie := range r.Cookies() {
		fmt.Println("Found a cookie named:", cookie.Name, cookie.Value)
		switch cookie.Name {
		case config.GetEnv().AccessTokenName:
			access = cookie.Value
		case config.GetEnv().RefreshTokenName:
			refresh = cookie.Value
		}
	}
	return access, refresh
}

func getUserFromId(id string) (*users.User, error) {
	fmt.Println("[INPUT_STRING]", id)
	decodeStr := stringutil.DecryptString(config.GetEnv().AppSecret, id)
	fmt.Println("[decodeStr:]", decodeStr)
	i, err := strconv.Atoi(decodeStr)
	if err != nil {
		fmt.Println(err)
	}
	dbUser, err := CheckUserInCache(i)
	if err != nil {
		return nil, errors.New("user not found")
	}
	return dbUser, nil
}

// ValidateToken validates token
func ValidateToken(token string) (jwtClaim map[string]interface{}, valide bool) {
	claim := jwt.MapClaims{}
	tkn, err := jwt.ParseWithClaims(
		token,
		claim,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(config.GetEnv().AppSecret), nil
		})
	if err != nil {
		fmt.Println("[ERROR]", err.Error())
		return nil, false
	}
	return claim, tkn.Valid
}

// Authentication for auth user
func Authentication(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		access, refresh := getTokensFromRequest(r)
		// check for access token
		if access != "" {
			claim, valid := ValidateToken(access)
			if !valid {
				fmt.Println("1")
				handleErr(r, w, errors.New("token not valid"), 403)
				return
			}
			fmt.Println(`claim["uid"]`, claim["uid"])
			user, err := getUserFromId(claim["uid"].(string))
			if err != nil {
				handleErr(r, w, err, 403)
				return
			}
			ctx := context.WithValue(r.Context(), RequestValueTypeUser, user)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else if refresh != "" {
			fmt.Println("entered 2", refresh)
			// is their is no access token check for refresh token
			// validate it, if valid the create new access and refresh token
			// and set it in cookie
			claim, valid := ValidateToken(refresh)
			if !valid {
				handleErr(r, w, errors.New("token not valid"), 403)
				return
			}
			fmt.Println(`claim["uid"]`, claim["uid"])
			user, err := getUserFromId(claim["uid"].(string))
			if err != nil {
				handleErr(r, w, err, 403)
				return
			}
			ctx := context.WithValue(r.Context(), RequestValueTypeUser, user)
			hash := stringutil.EncryptString(config.GetEnv().AppSecret, strconv.Itoa(user.ID))
			aT, err := tokens.CreateToken(hash, tokens.AccessToken)
			if err != nil {
				handleErr(r, w, err, 403)
				return
			}
			rT, err := tokens.CreateToken(hash, tokens.RefreshToken)
			if err != nil {
				handleErr(r, w, err, 403)
				return
			}
			http.SetCookie(w, &http.Cookie{Name: config.GetEnv().AccessTokenName, Value: aT, HttpOnly: true, Domain: config.GetEnv().CookieDomain, Path: "/"})
			http.SetCookie(w, &http.Cookie{Name: config.GetEnv().RefreshTokenName, Value: rT, HttpOnly: true, Domain: config.GetEnv().CookieDomain, Path: "/"})
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			handleErr(r, w, errors.New("access deined"), 403)
			return
		}
	}
}

func handleErr(r *http.Request, w http.ResponseWriter, err error, status int) {
	response.Response(response.ResponseObject{
		Req:    r,
		Res:    w,
		Data:   nil,
		Err:    err,
		Status: status,
	})
}

// Authorization to check the level of access to the user
func Authorization(next http.HandlerFunc, roles []users.UserType) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		for _, role := range roles {
			userData := ctx.Value(RequestValueTypeUser).(*users.User)
			if userData.Type == role {
				next.ServeHTTP(w, r)
				return
			}
		}
		handleErr(r, w, errors.New("access denied"), 403)
	}
}

func CheckUserInCache(userID int) (*users.User, error) {
	key, err := aero.NewKey("test", "aerospike", strconv.Itoa(userID))
	if err != nil {
		return nil, err
	}
	sessionUser := users.UserSessionObject{}
	err = db.ASClient.GetObject(nil, key, &sessionUser)
	if err != nil {
		return nil, err
	}
	fmt.Println(sessionUser.Data.Email)
	return sessionUser.Data, nil
}
