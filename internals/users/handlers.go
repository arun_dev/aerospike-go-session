package users

import (
	"net/http"

	"github.com/just-arun/aerospike-db-session/internals/response"
)

func getUsersHandler(w http.ResponseWriter, r *http.Request) {
	response.Response(response.ResponseObject{
		Req:    r,
		Res:    w,
		Data:   Users,
		Err:    nil,
		Status: http.StatusOK,
	})
}
