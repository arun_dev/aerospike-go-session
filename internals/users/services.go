package users

func Save(user User) (userId int) {
	Users = append(Users, &user)
	return user.ID
}

func GetOne(userName string) *User {
	for _, user := range Users {
		if user.Name == userName {
			return user
		}
	}
	return nil
}


func GetOneById(userID int) *User {
	for _, user := range Users {
		if user.ID == userID {
			return user
		}
	}
	return nil
}
