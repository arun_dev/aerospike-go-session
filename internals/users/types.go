package users

type UserType int

const (
	TypeClient UserType = iota
	TypeAdmin
)

type User struct {
	ID       int      `json:"id"`
	Name     string   `json:"name"`
	Email    string   `json:"email"`
	Type     UserType `json:"type"`
	Password string   `json:"password,omitempty"`
}

type UserSessionObject struct {
	Data *User
}
