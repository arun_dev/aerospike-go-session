package users

import "net/http"


type handlers struct {
	GetUsers http.HandlerFunc
}

func Handler() handlers {
	return handlers{
		GetUsers: getUsersHandler,
	}
}


