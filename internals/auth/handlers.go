package auth

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	aero "github.com/aerospike/aerospike-client-go"
	"github.com/just-arun/aerospike-db-session/internals/config"
	"github.com/just-arun/aerospike-db-session/internals/db"
	"github.com/just-arun/aerospike-db-session/internals/response"
	"github.com/just-arun/aerospike-db-session/internals/users"
)

func loginHandler(w http.ResponseWriter, r *http.Request) {
	var user LoginType
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		response.Response(response.ResponseObject{
			Req:    r,
			Res:    w,
			Data:   nil,
			Err:    err,
			Status: 409,
		})
		return
	}
	result, err := Login(user)
	if err != nil {
		response.Response(response.ResponseObject{
			Req:    r,
			Res:    w,
			Data:   nil,
			Err:    err,
			Status: 409,
		})
		return
	}
	fmt.Println(result.UserDetail)
	err = SaveUserSession(result.UserDetail)
	fmt.Println(err)
	http.SetCookie(w, &http.Cookie{Name: config.GetEnv().AccessTokenName, Value: result.AccessToken, HttpOnly: true, Domain: config.GetEnv().CookieDomain, Path: "/"})
	http.SetCookie(w, &http.Cookie{Name: config.GetEnv().RefreshTokenName, Value: result.RefreshToken, HttpOnly: true, Domain: config.GetEnv().CookieDomain, Path: "/"})
	response.Response(response.ResponseObject{
		Req:    r,
		Res:    w,
		Data:   result,
		Err:    nil,
		Status: 200,
	})
}

func SaveUserSession(user *users.User) error {
	key, err := aero.NewKey("test", "aerospike", strconv.Itoa(user.ID))
	if err != nil {
		return err
	}

	policy := aero.NewWritePolicy(0, 0)
	policy.TotalTimeout = time.Duration(config.GetEnv().RefreshTokenExpTime)

	// define some bins with data
	bins := &users.UserSessionObject{
		Data: user,
	}

	// write the bins
	err = db.ASClient.PutObject(policy, key, bins)
	if err != nil {
		return err
	}
	return nil
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	var user RegisterType
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		response.Response(response.ResponseObject{
			Req:    r,
			Res:    w,
			Data:   nil,
			Err:    err,
			Status: 409,
		})
		return
	}
	userID := Register(user)
	response.Response(response.ResponseObject{
		Req: r,
		Res: w,
		Data: map[string]interface{}{
			"id": userID,
		},
		Err:    nil,
		Status: 201,
	})
}
