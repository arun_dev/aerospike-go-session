package auth

import (
	"errors"
	"strconv"
	"time"

	"github.com/just-arun/aerospike-db-session/internals/config"
	"github.com/just-arun/aerospike-db-session/internals/stringutil"
	"github.com/just-arun/aerospike-db-session/internals/tokens"
	"github.com/just-arun/aerospike-db-session/internals/users"
)

func Login(user LoginType) (*loginResponse, error) {
	// get user form db
	dbUser := users.GetOne(user.Name)
	if dbUser == nil {
		return nil, errors.New("user not found")
	}

	// compare password
	passwordMatch := stringutil.CompareString(dbUser.Password, user.Password)
	if !passwordMatch {
		return nil, errors.New("invalid credentials")
	}

	// generate token
	tokens, err := tokens.GenerateToken(strconv.Itoa(dbUser.ID))
	if err != nil {
		return nil, err
	}
	dbUser.Password = ""
	return &loginResponse{
		UserDetail:   dbUser,
		AccessToken:  tokens.AccessToken,
		RefreshToken: tokens.RefreshToken,
	}, nil
}

func Register(register RegisterType) (userID int) {
	var user users.User
	user.Email = register.Email
	user.Name = register.Name
	user.Password = stringutil.EncryptString(config.GetEnv().AppSecret, register.Password)
	user.Type = users.TypeClient
	user.ID = int(time.Now().Unix())
	return users.Save(user)
}
