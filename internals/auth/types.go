package auth

import "github.com/just-arun/aerospike-db-session/internals/users"

type RegisterType struct {
	Email    string `json:"email"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type LoginType struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

// loginResponse type returned when login
type loginResponse struct {
	UserDetail   *users.User `json:"userDetail"`
	AccessToken  string      `json:"access"`
	RefreshToken string      `json:"refresh"`
}
