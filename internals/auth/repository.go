package auth

import "net/http"

type handlers struct {
	Login        http.HandlerFunc
	Register http.HandlerFunc
}

func Handler() handlers {
	return handlers{
		Login:        loginHandler,
		Register: registerHandler,
	}
}
