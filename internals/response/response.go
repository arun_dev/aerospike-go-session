package response

import (
	"encoding/json"
	"net/http"
)

type ResponseObject struct {
	Req    *http.Request
	Res    http.ResponseWriter
	Data   interface{}
	Err    error
	Status int
}

func errorFormate(err error) string {
	return `{"error":{"message":"` + err.Error() + `"}}`
}

// response middleware function
func Response(param ResponseObject) {
	if param.Err != nil {
		param.Res.WriteHeader(param.Status)
		param.Res.Write([]byte(errorFormate(param.Err)))
		return
	} else {
		data, err := json.Marshal(param.Data)
		if err != nil {
			param.Res.WriteHeader(param.Status)
			param.Res.Write([]byte(errorFormate(param.Err)))
			return
		}
		param.Res.WriteHeader(param.Status)
		param.Res.Write(data)
		return
	}
}
