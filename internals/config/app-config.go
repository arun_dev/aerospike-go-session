package config

import (
	"log"
	"os"
	"strconv"
	"time"

	"github.com/joho/godotenv"
)

type getEnv struct {
	Port                string
	CorsOrigin          string
	AeroSpikeDBHost     string
	AeroSpikeDBPort     int
	AppSecret           string
	AccessTokenName     string
	RefreshTokenName    string
	AccessTokenExpTime  int64
	RefreshTokenExpTime int64
	CookieDomain        string
}

func stringToInt(s string) int {
	i64, _ := strconv.ParseInt(s, 10, 32)
	return int(i64)
}

func GetEnv() getEnv {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
	return getEnv{
		Port:                os.Getenv("PORT"),
		CorsOrigin:          os.Getenv("CORS_ORIGIN"),
		AeroSpikeDBHost:     os.Getenv("AEROSPIKE_DB_HOST"),
		AeroSpikeDBPort:     stringToInt(os.Getenv("AEROSPIKE_DB_PORT")),
		AppSecret:           os.Getenv("APP_SECRET"),
		AccessTokenName:     os.Getenv("ACCESS_TOKEN_NAME"),
		RefreshTokenName:    os.Getenv("REFRESH_TOKEN_NAME"),
		AccessTokenExpTime:  time.Now().Add(time.Minute * time.Duration(stringToInt(os.Getenv("ACCESS_TOKEN_EXP")))).Unix(),
		RefreshTokenExpTime: time.Now().Add(time.Hour * time.Duration(stringToInt(os.Getenv("REFRESH_TOKEN_EXP")))).Unix(),
		CookieDomain:        os.Getenv("COOKIE_DOMAIN"),
	}
}
