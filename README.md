# AeroSpike DB Session
## objective
test AeroSpike db to if it suites our requirements for our up coming application, storing user non sensitive data with user id as key
## work process
- when user login, the user's id will be encrypted and stored in jwt token and send it to client as cookie
- while user tries to access auth endpoint the auth middleware will validate token and check for user detail in the AeroSpike db, is user details exists then they will be granted permission and sent to next middleware else they will get unauthorized error

## requirements
- go installed on the system or as a docker image
- AeroSpike installed on the system or as a docker image
- proper environment variable has to be provided in order for the application to run properly, go to `internals/config/app-config.go` file for env variable reference

## Available commands

| cmd          | operation                                                  |
| ------------ | ---------------------------------------------------------- |
| make dev     | starts a dev server                                        |
| make setup   | setup the dev environment                                  |
| make install | install the application in  the `$GOPATH`                  |
| make build   | build the application and place the binary file in bin dir |
| make clean   | clears all the binary file                                 |






