package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/just-arun/aerospike-db-session/cmd/routes"
	"github.com/just-arun/aerospike-db-session/internals/config"
	"github.com/just-arun/aerospike-db-session/internals/db"
	"github.com/just-arun/aerospike-db-session/internals/middlewares"
	"github.com/rs/cors"
)

func main() {
	fmt.Println("server starting...")
	// init dbs
	db.Init()

	r := chi.NewRouter()
	r.Use(middlewares.JsonHeader)
	// registering routes
	routes.AuthRoute(r)
	routes.UserRoute(r)

	// configure cors
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{config.GetEnv().CorsOrigin},
		AllowCredentials: true,
	})
	router := c.Handler(r)
	fmt.Printf("server started at port%v\n", config.GetEnv().Port)
	log.Fatal(http.ListenAndServe(config.GetEnv().Port, router))
}
