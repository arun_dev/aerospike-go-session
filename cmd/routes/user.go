package routes

import (
	"github.com/go-chi/chi/v5"
	"github.com/just-arun/aerospike-db-session/internals/middlewares"
	"github.com/just-arun/aerospike-db-session/internals/users"
)

func UserRoute(r *chi.Mux) {
	r.Route("/users", func(sr chi.Router) {
		sr.Get("/", middlewares.Authentication(
			users.Handler().GetUsers,
		))
	})
}
