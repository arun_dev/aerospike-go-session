package routes

import (
	"github.com/go-chi/chi/v5"
	"github.com/just-arun/aerospike-db-session/internals/auth"
)

func AuthRoute(r *chi.Mux) {
	// registering auth routes
	r.Route("/auth", func(sr chi.Router) {
		sr.Post("/register", auth.Handler().Register)
		sr.Post("/login", auth.Handler().Login)
	})
}
