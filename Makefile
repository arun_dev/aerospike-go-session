dev:
	go run cmd/aerospike-db-session/main.go
setup:
	export GOBIN=$(PWD)/bin
install:
	export GOBIN=$(PWD)/bin
	go install ./...
build:
	mkdir -p bin
	export GOBIN=$(PWD)/bin
	go build -o server ./cmd/aerospike-db-session/main.go
	mv server ./bin
run:
	./bin/server
clean:
	rm -rf ./main
	rm -rf ./bin/*